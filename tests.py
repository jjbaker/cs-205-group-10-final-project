"""
Automated test to run in gitlab CI/CD
Uses Arcade's headless mode to run tests

FROM ARCADE WIKI:
Arcade can render in headless mode on Linux servers with EGL installed.
This should work both in a desktop environment and on servers and even in virtual machines.
Both software and hardware rendering should be acceptable depending on your use case.

The test cannot currently run on gitlab because the server needs EGL installed to run headless
"""

# Required to run arcade in headless
import os

# Tests can be run manually on a normal computer by commenting this line
os.environ["ARCADE_HEADLESS"] = "True"

import time
from random import randrange

import arcade
from arcade.physics_engines import _move_sprite

from player import Player
from arcade import SpriteList, Sprite
from isometric.bullet import Bullet
from zombie import Zombie, ATTACK_RANGE
from isometric.iso_physics_engine import PhysicsEngineGame
from constants import LAYER_NAME_ENEMIES, LAYER_NAME_PLAYER, LAYER_NAME_BULLETS, SCREEN_WIDTH, SCREEN_HEIGHT

# Create a fake headless window
window = arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT)

# Create scene for tests
scene = arcade.Scene()

# Create wall list to test collisions
walls = SpriteList()

# Append wall to walls sprite list to test collisions
walls.append(Sprite(":resources:images/isometric_dungeon/stoneWall_W.png"))

# Add wall for collision tests
scene.add_sprite_list('Walls', use_spatial_hash=True, sprite_list=walls)

# Add player to scene to include player in tests
player = Player(scene)

# Setup sprite lists in scene
scene.add_sprite(LAYER_NAME_PLAYER, player)
scene.add_sprite_list_before(LAYER_NAME_ENEMIES, LAYER_NAME_PLAYER)
scene.add_sprite_list_before(LAYER_NAME_BULLETS, LAYER_NAME_ENEMIES)

# Create the 'physics engine'
physics_engine = PhysicsEngineGame(
    scene,
    player,
    dynamic_layers=[LAYER_NAME_ENEMIES],
    walls=scene["Walls"]
)


# Collision Tests
def test_collisions_bullet_zombie():
    # Test setup
    # Add zombie for test
    scene.add_sprite(LAYER_NAME_ENEMIES, Zombie((0, 100), 0))

    # Add bullet
    scene.add_sprite(LAYER_NAME_BULLETS, Bullet(0, 100, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 100, ui_element=False))

    # Collision test between first zombie and first bullet in scene
    collision_list = arcade.check_for_collision_with_list(scene[LAYER_NAME_ENEMIES][0], scene[LAYER_NAME_BULLETS])

    # Should have one collision
    assert len(collision_list) == 1

    # Cleanup scene
    scene.remove_sprite_list_by_name(LAYER_NAME_ENEMIES)
    scene.remove_sprite_list_by_name(LAYER_NAME_BULLETS)


def test_collisions_zombie_player():
    # Test setup
    # Add zombie for test
    scene.add_sprite(LAYER_NAME_ENEMIES, Zombie((0, 100), 0))

    player.set_position(10, 110)

    # Collision test between first zombie and player
    # Since distance is used to detect when zombies damage player, test it

    zombie = scene[LAYER_NAME_ENEMIES][0]
    assert (player.center_x - zombie.center_x) ** 2 + (player.center_y - zombie.center_y) ** 2 < ATTACK_RANGE

    # Cleanup scene
    scene.remove_sprite_list_by_name(LAYER_NAME_ENEMIES)


def test_collisions_zombie_wall():
    # Test setup
    # Move first wall to player
    walls[0].set_position(0, 100)

    player.set_position(10, 100)
    player.change_x = -10

    # Collision test between first zombie and wall
    # Calls physics engine move method to move the player toward the wall to see if collision happens
    collision_list = _move_sprite(player, [walls], ramp_up=False)

    # Should have one collision
    assert len(collision_list) == 1


def test_performance_zombies():
    # Add 1000 zombies to the scene
    print("Measure performance on creation of 1000 zombies...")
    start = time.time()

    for i in range(1000):
        scene.add_sprite(LAYER_NAME_ENEMIES, Zombie((randrange(-500, 500), randrange(-500, 500)), 0))

    end = time.time()
    print(f"1000 Zombies creation time: {end - start}s")

    print("Measure performance on 1 update call for 1000 zombies...")
    start = time.time()

    # Draw the zombies
    scene.draw(pixelated=True)

    for zombie in scene.get_sprite_list(LAYER_NAME_ENEMIES):
        zombie.update_with_player(player)

    scene.update_animation(
        0, [LAYER_NAME_ENEMIES]
    )

    end = time.time()
    print(f"1000 Zombies update time: {end - start}s")


print("Arcade Headless Tests")

"""
    Collision Tests
"""
test_collisions_bullet_zombie()

test_collisions_zombie_player()

test_collisions_zombie_wall()

"""
    Performance Tests
"""

test_performance_zombies()

print("All tests passed")
