# CS 205 Group 10 Final Project

Requires Python 3.10

Note:

    MAC USERS: The keyboard input is not registered at times - this is not a fixable bug, it is an issue with pyglet keyboard
    input. To resolve the error on the off chance it arrives, quit your IDE, reload and try again. Moreover, you will need to
    make sure to double-click "Start Game" to launch gameplay. The menu is less interactive on MacOS and requires
    some proactivity.

# 205 Days Later



Authors:

    Olin Ruppert-Bousquet
    Arthur Dolimier
    Josh Baker

Game Overview:

    This game is a 2.5D isometric zombie game with one objective: To survive as long as possible. Players are equipped
    with 25 rounds of ammunition and unlimited reloads. As time progresses, so does the difficulty... Zombie spawns, 
    speeds, and health will gradually increase over time, so be ready! Player health is represented by the red bar
    in the top left. If the player color changes to red, watch out! He doesn't have much time! Staying away from enemies
    will allow health to regenerate.

Install Information:
    
    This game requires Python 3.10 or higher
    Modules used in this game:
    arcade (latest release)
    pyinstaller

Game Setup (Recommended):

    Please download the appropriate executable from one of the links below.

    Drive cannot scan files so please be patient...

Note: Updated executables on 4/26/22 to account for exit code issue on game end -> pyinstaller error
Jason has approved this change with NO late penalty.

[Download Link for Windows](https://drive.google.com/uc?export=download&id=1iXSLp8eoakrbUJGz-7RJAhn2KQ37KtFS)

[Download Link for MacOS](https://drive.google.com/uc?export=download&id=1PvMHq_oNz0J0kxsIIYJvFXuelQNS2gKS)

Game Setup (Not running executable file):

    python --version  # For debugging 
    pip install virtualenv 
    virtualenv venv 
    source venv/bin/activate
    pip install -r requirements.txt

Game Controls (Movement):
    
    Players can move in directions Six directions: 
    North, North East, East, South East, South, South West, West, and North West using
    keyboard keys W A S D.

    Additionally, players can increase speed to sprint by pressing the left shift key

Game Controls (Shooting):
    
    Players can fire bullets at enemies by clicking on the point of the screen they wish to fire at. Bullets 
    have a set firing range of 1 second and then disapear. The gun holds 25 bullets which will auto reload 
    once empty. The player also has the option to manually reload by pressing the space bar.

Game Control Index:

    Key:                   Movement:
    W                   -> Move North
    A                   -> Move West
    S                   -> Move South
    D                   -> Move East
    LShift              -> Sprint
    Spacebar            -> Manual Reload
    Left Mouse click    -> Shoot bullet
    

Game Images:

    https://imgur.com/a/2b3g7bO

<blockquote class="imgur-embed-pub" lang="en" data-id="a/2b3g7bO"  >
<a href="//imgur.com/a/2b3g7bO">205 Days Later In game shots</a>
</blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>


CI/CD

    Builds fail due to the Gitlab server not having EGL installed, a requirement for running
    Arcade in headless mode. But tests have been created.

    https://api.arcade.academy/en/latest/advanced/headless.html?highlight=headless#id2

    Additionally, we have successful testing of executable builds in our CI/CD pipeline
    
    -> For more information, please reference test.py
    
