import time
from typing import Tuple
import arcade

from constants import LAYER_NAME_ENEMIES, LAYER_NAME_PLAYER, LAYER_NAME_BULLETS, GUN_DAMAGE
from isometric.iso_sprite import IsoSprite
from player import Player
import random
from zombie import Zombie
from zombie_cache import ZombieCache


class EnemyControl:

    def __init__(self, scene: arcade.scene, player: Player):
        # Use the scene to add enemies
        self.scene = scene

        self.spawn_timer = 0

        self.player = player

        self.marker_options = []

        for i in self.scene["Markers"]:
            if i.properties["type_marker"] == 1:
                self.marker_options.append(i)

        for x in range(50):
            self.elapsed_time = 0
            selection = random.choice(self.marker_options)
            x_offset = selection.center_x
            y_offset = selection.center_y
            # LRU Cache for spawn loc
            self.spawn_zombie((x_offset, y_offset), self.elapsed_time)

        self.kill_count = 0

        self.cache = ZombieCache()

        self.cache.set_cache_cap(6)

        self.t0 = time.time()

        self.spawn_proximity = 150000

        self.spawning_rate = 150

    def spawn_zombie(self, position: Tuple[int, int], time_elapsed: int):
        zombie = Zombie(position, time_elapsed)

        self.scene.add_sprite(LAYER_NAME_ENEMIES, zombie)

    def adjust_spawn_rate(self, elapsed_time, kill_count):

        # The rate changes linearly rather than incrementally, but follows a pattern of:
        # Increase spawn rate by 20ms every 30 seconds
        # Increase spawn rate by 20ms every 20 hits
        # Decrease spawn proximity by 100 every 1 hit

        # self.spawning_rate -= ((elapsed_time / 30) * 20)

        if self.spawning_rate >= 100:
            self.spawning_rate -= 5

        # self.spawning_rate -= ((kill_count / 20) * 20)

        self.spawn_proximity -= ((kill_count / 1) * 100)

        return self.spawning_rate

    def update(self):
        self.spawn_timer += 1

        # Get elapsed time since game started
        elapsed_time = time.time() - self.t0

        # Spawn timer
        for i in self.marker_options:
            if self.spawn_timer > self.spawning_rate:
                if (self.player.center_x - i.center_x) ** 2 + (
                        self.player.center_y - i.center_y) ** 2 < self.spawn_proximity:
                    x_offset = i.center_x
                    y_offset = i.center_y

                    # LRU Cache for spawn loc
                    if self.cache.get_from_cache(i) == -1:
                        self.spawn_zombie((x_offset, y_offset), elapsed_time)
                        self.spawn_timer = 0
                        self.cache.put_in_cache(i, 1)

                    else:
                        # Marker recently used - try again (recursive call prompts max recursion depth)
                        self.spawn_timer = self.spawning_rate - 1

        # Get all bullets on scene
        bullets = self.scene.get_sprite_list(LAYER_NAME_BULLETS)

        # Update zombie with player position
        for zombie in self.scene.get_sprite_list(LAYER_NAME_ENEMIES):
            zombie.update_with_player(self.player)

            # Zombie collisions with bullets
            collision_list = arcade.check_for_collision_with_list(zombie, bullets)
            if collision_list:
                # Kill the bullet if zombie if alive, if the zombie is in dying animation bullet goes through
                if zombie.is_alive():
                    collision_list[0].kill()

                # Damage zombie
                has_died = zombie.damage(GUN_DAMAGE)

                # If zombie died increase kill count
                if has_died and not zombie.marked_dead:
                    self.kill_count += 1

                    # max spawn rate of every 20 ms
                    if self.spawning_rate >= 20:
                        self.spawning_rate = self.adjust_spawn_rate(elapsed_time, self.kill_count)

                    zombie.marked_dead = True
