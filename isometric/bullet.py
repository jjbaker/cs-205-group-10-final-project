import math
import time
from isometric.utils import*
from constants import*

"""
    Class generates a bullet sprite and adds it to the scene
    based on starting and ending coordinates, the angle of the 
    bullet is determined.
    
    Additionally, this class is utilized for drawing bullets 
    representing remaining ammo in in_game_ui.py
    
    Child of arcade.Sprite 
"""


class Bullet(arcade.Sprite):

    def __init__(self, start_x, start_y, camera_width, camera_height, dest_x, dest_y, ui_element):

        if not ui_element:
            super().__init__(filename="assets/bullet.png", scale=0.75)

            # Set bullet center
            self.center_x = start_x
            self.center_y = start_y - 5

            # Determine bullet angle
            camera_x = camera_width / 2
            camera_y = camera_height / 2

            x_diff = dest_x - camera_x
            y_diff = dest_y - camera_y

            angle = math.atan2(y_diff, x_diff)
            self.angle = math.degrees(angle)

            # Set trajectory
            self.change_x = math.cos(angle) * BULLET_SPEED
            self.change_y = math.sin(angle) * BULLET_SPEED
            self.start_time = time.time()
            #print(f"Bullet angle: {self.angle:.2f}")

            self.active = True

            # Record shoot time
            self.shoot_time = time.time()
        else:
            super().__init__(filename="assets/good_bullet.png", scale=.08)
            self.angle = -90
            self.center_x = start_x
            self.center_y = start_y


