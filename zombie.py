import math
import random
from enum import Enum
from typing import Tuple

import arcade

from isometric.iso_sprite import Direction, FrameSequence, State, SpriteSheetData, IsoSprite

# Frames states order
from player import Player

ZOMBIE_ANIM_FRAME_ORDER = [
    FrameSequence(4, State.Idle),
    FrameSequence(8, State.Walking),
    FrameSequence(5, State.Attack),
    FrameSequence(6, State.Spawn),
    FrameSequence(8, State.Death),
]

ZOMBIE_ANIM_DIRECTIONS_ORDER = [
    Direction.W, Direction.NW, Direction.N, Direction.NE, Direction.E, Direction.SE, Direction.S, Direction.SW
]

# Distance for zombies to move to the player in distance^2
AGGRO_RANGE = 240000
STUCK_RANGE = 1
ATTACK_RANGE = 1500
ATTACK_SPEED = 50
MAX_HEALTH = 10
BASE_SPEED = 0.0019


class Action(Enum):
    IDLE = 0
    WANDER = 1
    MOVE = 2
    ATTACK = 3
    DEATH = 4
    SPAWN = 5


"""
TODO: Change init to spawn zombies at one of open spawn locations depending on kill count variable

As new kills are accumulated, new tiles are available for spawning
"""


class Zombie(IsoSprite):

    def __init__(self, position: Tuple[int, int], time_elapsed):
        super().__init__(position, SpriteSheetData("assets/animations/zombie-anim.png", ZOMBIE_ANIM_FRAME_ORDER,
                                                   ZOMBIE_ANIM_DIRECTIONS_ORDER, col_count=16, frame_duration=250), (78, 90), scale=1.6)
        self.time_elapsed = time_elapsed

        # Zombie health
        # Increase health linearly
        self.health = MAX_HEALTH + time_elapsed / 20 # 15s

        # Brain counter to update zombie ai loop
        self.brain_time = 0

        # Used for zombie attack speed
        self.attack_time = 0

        # Used for kill count
        self.marked_dead = False

        # Initialize
        self.current_action = Action.SPAWN
        self.current_direction = random.choice(list(Direction))

        # Lock current state to Spawn until spawn animation is done
        self.set_state(State.Spawn, True)

        self.goal: Tuple[int, int] = (0, 0)

        # Used to check if zombie is stuck behind a wall
        self.prev_pos: Tuple[int, int] = (0, 0)

        p1 = (-15, -25)
        p2 = (1, -25)
        p3 = (1, 13)
        p4 = (-15, 13)

        # set zombie hit box using points
        self.set_hit_box([p1, p2, p3, p4])

    def damage(self, damage: int) -> bool:
        """
        Damages the zombie

        :param damage: damage to deal the zombie
        :return: True if zombie died, False if still alive after shot
        """

        self.health -= damage

        # self.health = max(min(self.health, MAX_HEALTH), 0)

        if self.health <= 0:
            self.on_action_changed(Action.DEATH, (0.0, 0.0))
            return True

        return False

    def is_alive(self):
        return self.health > 0

    def on_animation_done(self, anim: Tuple[Direction, State]) -> Tuple[Direction, State]:
        anim_state_done = anim[1]
        anim_direction = anim[0]

        match anim_state_done:
            case State.Death:
                # Death animation is done, kill the entity
                self.kill()

            case State.Spawn:
                # after spawn animation is done change to idle
                self.set_state(State.Idle, False)

                # # Unlocks state to be able to be changed freely
                # self.unlock_state()

                # Return new key to update animation instantly
                return anim_direction, self.current_state

        # Was not changed so return the same
        return anim

    def update_with_player(self, player: Player):

        self.brain_time += 1
        self.attack_time += 1

        # print((player.center_x - self.center_x) ** 2 + (player.center_y - self.center_y))

        if self.brain_time > 150 and self.health > 0:

            if (player.center_x - self.center_x) ** 2 + (player.center_y - self.center_y) ** 2 < AGGRO_RANGE + (self.time_elapsed * 2) ** 2:
                # Player in range, move or attack

                # If close enough, attack
                if (player.center_x - self.center_x) ** 2 + (player.center_y - self.center_y) ** 2 < ATTACK_RANGE:
                    self.on_action_changed(Action.ATTACK, player.position)

                else:
                    # Else move toward player
                    self.on_action_changed(Action.MOVE, player.position)

            else:
                # No player in range, wander or idle
                new_action = random.choices(list(Action), weights=(0.5, 0.5, 0.0, 0.0, 0.0, 0.0), k=1)[0]

                # Action changed = New Goal
                self.on_action_changed(new_action, player.position)

            # Reset brain_time
            self.brain_time = 0

        self.action_update(player)

    def on_action_changed(self, action: Action, player_pos: Tuple[float, float]):
        if self.is_state_locked():
            # Don't change action if state is locked
            return

        self.current_action = action

        match action:
            case Action.WANDER:
                self.goal = (
                    self.center_x + random.randrange(-80, 80, 1),
                    self.center_y + random.randrange(-80, 80, 1)
                )
            case Action.IDLE:
                # Reset movement to avoid sliding
                self.change_x = 0
                self.change_y = 0

            case Action.DEATH:
                # Reset movement to avoid sliding
                self.change_x = 0
                self.change_y = 0

            # case Action.MOVE:
            #     if (self.prev_pos[0] + STUCK_RANGE > self.center_x > self.prev_pos[0] - STUCK_RANGE) and (
            #             self.prev_pos[1] + STUCK_RANGE > self.center_y > self.prev_pos[1] - STUCK_RANGE):
            #         # self.color = arcade.csscolor.GREEN
            #         self.on_action_changed(Action.WANDER, player_pos)
            #
            #     else:
            #         # self.color = arcade.csscolor.BLUE
            #         self.prev_pos = (self.center_x, self.center_y)

    def action_update(self, player: Player):
        match self.current_action:
            case Action.DEATH:
                self.set_state(State.Death)

            case Action.MOVE:
                self.set_state(State.Walking)

                lerp_pos = arcade.lerp_vec((self.center_x, self.center_y), player.position, BASE_SPEED + self.time_elapsed / 250000)
                # print("current speed: ", BASE_SPEED + self.time_elapsed / 300000)

                dir_x, dir_y = self.look_at_vector(player.position)

                self.set_direction((dir_x, dir_y))

                # Works with physics engine
                self.change_x = dir_x * abs(self.center_x - lerp_pos[0])
                self.change_y = dir_y * abs(self.center_y - lerp_pos[1])

                # Stops lerping to the goal if he close to the goal
                if (player.center_x - self.center_x) ** 2 + (player.center_y - self.center_y) ** 2 < ATTACK_RANGE:
                    # For some reason have to reset these too
                    self.change_x = 0
                    self.change_y = 0

                    # Switch directly to attacking since close to player
                    self.current_action = Action.ATTACK

            case Action.ATTACK:
                # self.color = arcade.color.BLUE
                self.set_state(State.Attack)

                if self.attack_time > ATTACK_SPEED:
                    # Hit player
                    player.player_hit()

                    # Reset attack
                    self.attack_time = 0

                # If close enough, attack
                if (player.center_x - self.center_x) ** 2 + (player.center_y - self.center_y) ** 2 > ATTACK_RANGE:

                    # Player has gone outside attack range, go back to moving
                    self.current_action = Action.MOVE

            case Action.WANDER:
                # self.color = arcade.color.GREEN
                self.set_state(State.Walking)

                lerp_pos = arcade.lerp_vec((self.center_x, self.center_y), self.goal, 0.005)

                dir_x = 1 if lerp_pos[0] > self.center_x else -1
                dir_y = 1 if lerp_pos[1] > self.center_y else -1

                self.set_direction((dir_x, dir_y))

                # Works with physics engine
                self.change_x = dir_x * abs(self.center_x - lerp_pos[0])
                self.change_y = dir_y * abs(self.center_y - lerp_pos[1])

                # Stops lerping to the goal if he close to the goal
                if (self.goal[0] - self.center_x) ** 2 + (self.goal[1] - self.center_y) ** 2 < 64:
                    # For some reason have to reset these too
                    self.change_x = 0
                    self.change_y = 0

                    # Reset back to IDLE action
                    self.current_action = Action.IDLE

            case Action.IDLE:
                self.set_state(State.Idle)
