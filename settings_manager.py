"""
Stores settings to be used everywhere in the game
These are modified in the settings view
"""

settings = {}

# music and sound volume percentage, scale 0.0 -> 1.0
volume: float = 1.0

# Whether the game is fullscreen
fullscreen: bool = False

settings['volume'] = volume

settings['fullscreen'] = fullscreen
