import math
import os
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Union, cast, Callable
import os

import arcade
import pytiled_parser
import pytiled_parser.tiled_object
from arcade import (
    AnimatedTimeBasedSprite,
    AnimationKeyframe,
    Sprite,
    SpriteList,
    load_texture,
)
from arcade.arcade_types import Point, TiledObject
from arcade.geometry_generic import rotate_point
from arcade.tilemap.tilemap import _get_image_info_from_tileset, _get_image_source
from pyglet.math import Vec2

from arcade import (
    AnimatedTimeBasedSprite,
    AnimationKeyframe,
    Sprite,
    SpriteList,
    load_texture,
)


class IsometricTileMap(arcade.TileMap):

    def __init__(self, map_file: Union[str, Path] = "", scaling: float = 1.0,
                 layer_options: Optional[Dict[str, Dict[str, Any]]] = None, use_spatial_hash: Optional[bool] = None,
                 hit_box_algorithm: str = "Simple", hit_box_detail: float = 4.5,
                 tiled_map: Optional[pytiled_parser.TiledMap] = None, offset: Vec2 = Vec2(0, 0),
                 callback_progress: Callable = None):

        self.tiles_processed = 0

        self.callback_progress = callback_progress

        super().__init__(map_file, scaling, layer_options, use_spatial_hash, hit_box_algorithm, hit_box_detail,
                         tiled_map, offset)


    def _process_tile_layer(
        self,
        layer: pytiled_parser.TileLayer,
        scaling: float = 1.0,
        use_spatial_hash: Optional[bool] = None,
        hit_box_algorithm: str = "Simple",
        hit_box_detail: float = 4.5,
        offset: Vec2 = Vec2(0, 0),
        custom_class: Optional[type] = None,
        custom_class_args: Dict[str, Any] = {},
    ) -> SpriteList:

        sprite_list: SpriteList = SpriteList(use_spatial_hash=use_spatial_hash, is_static=True)
        map_array = layer.data

        if layer.properties is not None and 'hidden' in layer.properties:
            sprite_list.visible = not layer.properties['hidden']
            print(f"Set layer {layer.name} to visible={sprite_list.visible}")
        else:
            # if hidden custom data is not set use layer visibility
            sprite_list.visible = layer.visible

        # Loop through the layer and add in the list
        for row_index, row in enumerate(map_array):
            for column_index, item in enumerate(row):
                # Check for an empty tile
                if item == 0:
                    continue

                tile = self._get_tile_by_gid(item)
                if tile is None:
                    raise ValueError(
                        (
                            f"Couldn't find tile for item {item} in layer "
                            f"'{layer.name}' in file '{self.tiled_map.map_file}'"
                            f"at ({column_index}, {row_index})."
                        )
                    )

                my_sprite = self._create_sprite_from_tile(
                    tile,
                    scaling=scaling,
                    hit_box_algorithm=hit_box_algorithm,
                    hit_box_detail=hit_box_detail,
                    custom_class=custom_class,
                    custom_class_args=custom_class_args,
                )

                if my_sprite is None:
                    print(
                        f"Warning: Could not create sprite number {item} in layer '{layer.name}' {tile.image}"
                    )
                else:
                    """
                    Cast to isometric coords
                    """
                    screen_width = (self.tiled_map.map_size.width * scaling)
                    screen_height = (self.tiled_map.map_size.height * scaling)

                    # Screen coordinates
                    screen_x = column_index - screen_width / 2
                    screen_y = row_index - screen_height / 2

                    # because the sprites are already cast to the ~30 degrees for the isometric the only needed
                    # rotations is the 45 degrees. However since cos and sin 45 are both 0.707 they are removed from
                    # the system as it simply makes the cast smaller
                    iso_x = (screen_x - screen_y) * ((self.tiled_map.tile_size.width * scaling) / 2)
                    iso_y = -(screen_x + screen_y) * ((self.tiled_map.tile_size.height * scaling) / 2)

                    my_sprite.set_position(iso_x, iso_y)

                    # Tint
                    if layer.tint_color:
                        my_sprite.color = layer.tint_color

                    # Opacity
                    opacity = layer.opacity
                    if opacity:
                        my_sprite.alpha = int(opacity * 255)

                    self.tiles_processed += 1

                    if self.callback_progress is not None:
                        self.callback_progress(self.tiles_processed, layer.properties['totalCount'])

                    sprite_list.append(my_sprite)

        if layer.properties:
            sprite_list.properties = layer.properties

        return sprite_list

    def _process_object_layer(
        self,
        layer: pytiled_parser.ObjectLayer,
        scaling: float = 1.0,
        use_spatial_hash: Optional[bool] = None,
        hit_box_algorithm: str = "Simple",
        hit_box_detail: float = 4.5,
        offset: Vec2 = Vec2(0, 0),
        custom_class: Optional[type] = None,
        custom_class_args: Dict[str, Any] = {},
    ) -> Tuple[Optional[SpriteList], Optional[List[TiledObject]]]:

        if not scaling:
            scaling = self.scaling

        sprite_list: Optional[SpriteList] = None
        objects_list: Optional[List[TiledObject]] = []

        for cur_object in layer.tiled_objects:
            # shape: Optional[Union[Point, PointList, Rect]] = None
            if isinstance(cur_object, pytiled_parser.tiled_object.Tile):
                if not sprite_list:
                    sprite_list = SpriteList(use_spatial_hash=use_spatial_hash)

                tile = self._get_tile_by_gid(cur_object.gid)
                my_sprite = self._create_sprite_from_tile(
                    tile,
                    scaling=scaling,
                    hit_box_algorithm=hit_box_algorithm,
                    hit_box_detail=hit_box_detail,
                    custom_class=custom_class,
                    custom_class_args=custom_class_args,
                )

                x = (cur_object.coordinates.x * scaling) + offset[0]
                y = (
                    (
                        self.tiled_map.map_size.height * self.tiled_map.tile_size[1]
                        - cur_object.coordinates.y
                    )
                    * scaling
                ) + offset[1]

                my_sprite.width = width = cur_object.size[0] * scaling
                my_sprite.height = height = cur_object.size[1] * scaling
                # center_x = width / 2
                # center_y = height / 2
                if cur_object.rotation:
                    rotation = -math.radians(cur_object.rotation)
                else:
                    rotation = 0

                angle_degrees = math.degrees(rotation)
                rotated_center_x, rotated_center_y = rotate_point(
                    width / 2, height / 2, 0, 0, angle_degrees
                )

                my_sprite.position = (x + rotated_center_x, y + rotated_center_y)
                my_sprite.angle = angle_degrees

                if layer.tint_color:
                    my_sprite.color = layer.tint_color

                opacity = layer.opacity
                if opacity:
                    my_sprite.alpha = int(opacity * 255)

                if cur_object.properties and "change_x" in cur_object.properties:
                    my_sprite.change_x = float(cur_object.properties["change_x"])

                if cur_object.properties and "change_y" in cur_object.properties:
                    my_sprite.change_y = float(cur_object.properties["change_y"])

                if cur_object.properties and "boundary_bottom" in cur_object.properties:
                    my_sprite.boundary_bottom = float(
                        cur_object.properties["boundary_bottom"]
                    )

                if cur_object.properties and "boundary_top" in cur_object.properties:
                    my_sprite.boundary_top = float(
                        cur_object.properties["boundary_top"]
                    )

                if cur_object.properties and "boundary_left" in cur_object.properties:
                    my_sprite.boundary_left = float(
                        cur_object.properties["boundary_left"]
                    )

                if cur_object.properties and "boundary_right" in cur_object.properties:
                    my_sprite.boundary_right = float(
                        cur_object.properties["boundary_right"]
                    )

                if cur_object.properties:
                    my_sprite.properties.update(cur_object.properties)

                if cur_object.type:
                    my_sprite.properties["type"] = cur_object.type

                if cur_object.name:
                    my_sprite.properties["name"] = cur_object.name

                sprite_list.visible = layer.visible
                sprite_list.append(my_sprite)
                continue
            elif isinstance(cur_object, pytiled_parser.tiled_object.Point):
                x = cur_object.coordinates.x * scaling
                y = (
                    self.tiled_map.map_size.height * self.tiled_map.tile_size[1]
                    - cur_object.coordinates.y
                ) * scaling

                shape = [x + offset[0], y + offset[1]]
            elif isinstance(cur_object, pytiled_parser.tiled_object.Rectangle):
                x = cur_object.coordinates.x + offset[0]
                y = cur_object.coordinates.y + offset[1]
                sx = x
                sy = -y
                ex = x + cur_object.size.width
                ey = -(y + cur_object.size.height)

                p1 = [sx, sy]
                p2 = [ex, sy]
                p3 = [ex, ey]
                p4 = [sx, ey]

                shape = [p1, p2, p3, p4]
            elif isinstance(
                cur_object, pytiled_parser.tiled_object.Polygon
            ) or isinstance(cur_object, pytiled_parser.tiled_object.Polyline):
                shape = []
                for point in cur_object.points:
                    x = point.x + cur_object.coordinates.x
                    y = (self.height * self.tile_height) - (
                        point.y + cur_object.coordinates.y
                    )
                    point = (x + offset[0], y + offset[1])
                    shape.append(point)

                # If shape is a polyline, and it is closed, we need to remove the duplicate end point
                if shape[0][0] == shape[-1][0] and shape[0][1] == shape[-1][1]:
                    shape.pop()
            elif isinstance(cur_object, pytiled_parser.tiled_object.Ellipse):
                hw = cur_object.size.width / 2
                hh = cur_object.size.height / 2
                cx = cur_object.coordinates.x + hw
                cy = cur_object.coordinates.y + hh

                total_steps = 8
                angles = [
                    step / total_steps * 2 * math.pi for step in range(total_steps)
                ]
                shape = []
                for angle in angles:
                    x = hw * math.cos(angle) + cx
                    y = -(hh * math.sin(angle) + cy)
                    point = [x + offset[0], y + offset[1]]
                    shape.append(point)
            else:
                continue

            if shape:
                tiled_object = TiledObject(
                    shape, cur_object.properties, cur_object.name, cur_object.type
                )

                if not objects_list:
                    objects_list = []

                objects_list.append(tiled_object)

        return sprite_list or None, objects_list or None

    def _create_sprite_from_tile(
            self,
            tile: pytiled_parser.Tile,
            scaling: float = 1.0,
            hit_box_algorithm: str = "Simple",
            hit_box_detail: float = 4.5,
            custom_class: Optional[type] = None,
            custom_class_args: Dict[str, Any] = {},
    ) -> Sprite:
        """Given a tile from the parser, try and create a Sprite from it."""

        # --- Step 1, Find a reference to an image this is going to be based off of
        map_source = self.tiled_map.map_file
        map_directory = os.path.dirname(map_source)
        image_file = _get_image_source(tile, map_directory)

        if tile.animation:
            if not custom_class:
                custom_class = AnimatedTimeBasedSprite
            elif not issubclass(custom_class, AnimatedTimeBasedSprite):
                raise RuntimeError(
                    f"""
                    Tried to use a custom class {custom_class.__name__} for animated tiles
                    that doesn't subclass AnimatedTimeBasedSprite.
                    Custom classes for animated tiles must subclass AnimatedTimeBasedSprite.
                    """
                )
            # print(custom_class.__name__)
            args = {"filename": image_file, "scale": scaling}
            my_sprite = custom_class(**custom_class_args, **args)  # type: ignore
        else:
            if not custom_class:
                custom_class = Sprite
            elif not issubclass(custom_class, Sprite):
                raise RuntimeError(
                    f"""
                    Tried to use a custom class {custom_class.__name__} for
                    a tile that doesn't subclass arcade.Sprite.
                    Custom classes for tiles must subclass arcade.Sprite.
                    """
                )
            image_x, image_y, width, height = _get_image_info_from_tileset(tile)
            args = {
                "filename": image_file,
                "scale": scaling,
                "image_x": image_x,
                "image_y": image_y,
                "image_width": width,
                "image_height": height,
                "flipped_horizontally": tile.flipped_horizontally,
                "flipped_vertically": tile.flipped_vertically,
                "flipped_diagonally": tile.flipped_diagonally,
                "hit_box_algorithm": hit_box_algorithm,  # type: ignore
                "hit_box_detail": hit_box_detail,
            }
            my_sprite = custom_class(**custom_class_args, **args)  # type: ignore

        if tile.tileset.tiles is not None and tile.tileset.tiles.get(tile.id) is not None and tile.tileset.tiles.get(tile.id).properties is not None:
            for key, value in tile.tileset.tiles.get(tile.id).properties.items():
                my_sprite.properties[key] = value

        if tile.type:
            my_sprite.properties["type"] = tile.type

        if tile.objects is not None:
            if not isinstance(tile.objects, pytiled_parser.ObjectLayer):
                print("Warning, tile.objects is not an ObjectLayer as expected.")
                return my_sprite

            if len(tile.objects.tiled_objects) > 1:
                if tile.image:
                    print(
                        f"Warning, only one hit box supported for tile with image {tile.image}."
                    )
                else:
                    print("Warning, only one hit box supported for tile.")

            for hitbox in tile.objects.tiled_objects:
                points: List[Point] = []
                if isinstance(hitbox, pytiled_parser.tiled_object.Rectangle):
                    if hitbox.size is None:
                        print(
                            "Warning: Rectangle hitbox created for without a "
                            "height or width Ignoring."
                        )
                        continue

                    sx = hitbox.coordinates.x - (my_sprite.width / (scaling * 2))
                    sy = -(hitbox.coordinates.y - (my_sprite.height / (scaling * 2)))
                    ex = (hitbox.coordinates.x + hitbox.size.width) - (
                            my_sprite.width / (scaling * 2)
                    )
                    # issue #1068
                    # fixed size of rectangular hitbox
                    ey = -(hitbox.coordinates.y + hitbox.size.height) + (
                            my_sprite.height / (scaling * 2)
                    )

                    points = [[sx, sy], [ex, sy], [ex, ey], [sx, ey]]
                elif isinstance(
                        hitbox, pytiled_parser.tiled_object.Polygon
                ) or isinstance(hitbox, pytiled_parser.tiled_object.Polyline):
                    for point in hitbox.points:
                        adj_x = (
                                point.x
                                + hitbox.coordinates.x
                                - my_sprite.width / (scaling * 2)
                        )
                        adj_y = -(
                                point.y
                                + hitbox.coordinates.y
                                - my_sprite.height / (scaling * 2)
                        )
                        adj_point = [adj_x, adj_y]
                        points.append(adj_point)

                    if points[0][0] == points[-1][0] and points[0][1] == points[-1][1]:
                        points.pop()
                elif isinstance(hitbox, pytiled_parser.tiled_object.Ellipse):
                    if not hitbox.size:
                        print(
                            f"Warning: Ellipse hitbox created without a height "
                            f" or width for {tile.image}. Ignoring."
                        )
                        continue

                    hw = hitbox.size.width / 2
                    hh = hitbox.size.height / 2
                    cx = hitbox.coordinates.x + hw
                    cy = hitbox.coordinates.y + hh

                    acx = cx - (my_sprite.width / (scaling * 2))
                    acy = cy - (my_sprite.height / (scaling * 2))

                    total_steps = 8
                    angles = [
                        step / total_steps * 2 * math.pi for step in range(total_steps)
                    ]
                    for angle in angles:
                        x = hw * math.cos(angle) + acx
                        y = -(hh * math.sin(angle) + acy)
                        points.append([x, y])
                else:
                    print(f"Warning: Hitbox type {type(hitbox)} not supported.")

                if tile.flipped_vertically:
                    for point in points:
                        point[1] *= -1

                if tile.flipped_horizontally:
                    for point in points:
                        point[0] *= -1

                if tile.flipped_diagonally:
                    for point in points:
                        point[0], point[1] = point[1], point[0]

                my_sprite.hit_box = points

        if tile.animation:
            key_frame_list = []
            for frame in tile.animation:
                frame_tile = self._get_tile_by_id(tile.tileset, frame.tile_id)
                if frame_tile:
                    image_file = _get_image_source(frame_tile, map_directory)

                    if frame_tile.image and image_file:
                        texture = load_texture(image_file)
                    elif not frame_tile.image and image_file:
                        # No image for tile, pull from tilesheet
                        (
                            image_x,
                            image_y,
                            width,
                            height,
                        ) = _get_image_info_from_tileset(frame_tile)

                        texture = load_texture(
                            image_file, image_x, image_y, width, height
                        )
                    else:
                        raise RuntimeError(
                            f"Warning: failed to load image for animation frame for "
                            f"tile '{frame_tile.id}', '{image_file}'."
                        )

                    key_frame = AnimationKeyframe(  # type: ignore
                        frame.tile_id, frame.duration, texture
                    )
                    key_frame_list.append(key_frame)

                    if len(key_frame_list) == 1:
                        my_sprite.texture = key_frame.texture

            cast(AnimatedTimeBasedSprite, my_sprite).frames = key_frame_list

        return my_sprite

    def _create_sprite_from_tile(
            self,
            tile: pytiled_parser.Tile,
            scaling: float = 1.0,
            hit_box_algorithm: str = "Simple",
            hit_box_detail: float = 4.5,
            custom_class: Optional[type] = None,
            custom_class_args: Dict[str, Any] = {},
    ) -> Sprite:
        """Given a tile from the parser, try and create a Sprite from it."""

        # --- Step 1, Find a reference to an image this is going to be based off of
        map_source = self.tiled_map.map_file
        map_directory = os.path.dirname(map_source)
        image_file = _get_image_source(tile, map_directory)

        if tile.animation:
            if not custom_class:
                custom_class = AnimatedTimeBasedSprite
            elif not issubclass(custom_class, AnimatedTimeBasedSprite):
                raise RuntimeError(
                    f"""
                    Tried to use a custom class {custom_class.__name__} for animated tiles
                    that doesn't subclass AnimatedTimeBasedSprite.
                    Custom classes for animated tiles must subclass AnimatedTimeBasedSprite.
                    """
                )
            # print(custom_class.__name__)
            args = {"filename": image_file, "scale": scaling}
            my_sprite = custom_class(**custom_class_args, **args)  # type: ignore
        else:
            if not custom_class:
                custom_class = Sprite
            elif not issubclass(custom_class, Sprite):
                raise RuntimeError(
                    f"""
                    Tried to use a custom class {custom_class.__name__} for
                    a tile that doesn't subclass arcade.Sprite.
                    Custom classes for tiles must subclass arcade.Sprite.
                    """
                )
            image_x, image_y, width, height = _get_image_info_from_tileset(tile)
            args = {
                "filename": image_file,
                "scale": scaling,
                "image_x": image_x,
                "image_y": image_y,
                "image_width": width,
                "image_height": height,
                "flipped_horizontally": tile.flipped_horizontally,
                "flipped_vertically": tile.flipped_vertically,
                "flipped_diagonally": tile.flipped_diagonally,
                "hit_box_algorithm": hit_box_algorithm,  # type: ignore
                "hit_box_detail": hit_box_detail,
            }
            my_sprite = custom_class(**custom_class_args, **args)  # type: ignore

        if tile.tileset.tiles is not None and tile.tileset.tiles.get(tile.id) is not None and tile.tileset.tiles.get(tile.id).properties is not None:
            for key, value in tile.tileset.tiles.get(tile.id).properties.items():
                my_sprite.properties[key] = value

        if tile.type:
            my_sprite.properties["type"] = tile.type

        if tile.objects is not None:
            if not isinstance(tile.objects, pytiled_parser.ObjectLayer):
                print("Warning, tile.objects is not an ObjectLayer as expected.")
                return my_sprite

            if len(tile.objects.tiled_objects) > 1:
                if tile.image:
                    print(
                        f"Warning, only one hit box supported for tile with image {tile.image}."
                    )
                else:
                    print("Warning, only one hit box supported for tile.")

            for hitbox in tile.objects.tiled_objects:
                points: List[Point] = []
                if isinstance(hitbox, pytiled_parser.tiled_object.Rectangle):
                    if hitbox.size is None:
                        print(
                            "Warning: Rectangle hitbox created for without a "
                            "height or width Ignoring."
                        )
                        continue

                    sx = hitbox.coordinates.x - (my_sprite.width / (scaling * 2))
                    sy = -(hitbox.coordinates.y - (my_sprite.height / (scaling * 2)))
                    ex = (hitbox.coordinates.x + hitbox.size.width) - (
                            my_sprite.width / (scaling * 2)
                    )
                    # issue #1068
                    # fixed size of rectangular hitbox
                    ey = -(hitbox.coordinates.y + hitbox.size.height) + (
                            my_sprite.height / (scaling * 2)
                    )

                    points = [[sx, sy], [ex, sy], [ex, ey], [sx, ey]]
                elif isinstance(
                        hitbox, pytiled_parser.tiled_object.Polygon
                ) or isinstance(hitbox, pytiled_parser.tiled_object.Polyline):
                    for point in hitbox.points:
                        adj_x = (
                                point.x
                                + hitbox.coordinates.x
                                - my_sprite.width / (scaling * 2)
                        )
                        adj_y = -(
                                point.y
                                + hitbox.coordinates.y
                                - my_sprite.height / (scaling * 2)
                        )
                        adj_point = [adj_x, adj_y]
                        points.append(adj_point)

                    if points[0][0] == points[-1][0] and points[0][1] == points[-1][1]:
                        points.pop()
                elif isinstance(hitbox, pytiled_parser.tiled_object.Ellipse):
                    if not hitbox.size:
                        print(
                            f"Warning: Ellipse hitbox created without a height "
                            f" or width for {tile.image}. Ignoring."
                        )
                        continue

                    hw = hitbox.size.width / 2
                    hh = hitbox.size.height / 2
                    cx = hitbox.coordinates.x + hw
                    cy = hitbox.coordinates.y + hh

                    acx = cx - (my_sprite.width / (scaling * 2))
                    acy = cy - (my_sprite.height / (scaling * 2))

                    total_steps = 8
                    angles = [
                        step / total_steps * 2 * math.pi for step in range(total_steps)
                    ]
                    for angle in angles:
                        x = hw * math.cos(angle) + acx
                        y = -(hh * math.sin(angle) + acy)
                        points.append([x, y])
                else:
                    print(f"Warning: Hitbox type {type(hitbox)} not supported.")

                if tile.flipped_vertically:
                    for point in points:
                        point[1] *= -1

                if tile.flipped_horizontally:
                    for point in points:
                        point[0] *= -1

                if tile.flipped_diagonally:
                    for point in points:
                        point[0], point[1] = point[1], point[0]

                my_sprite.hit_box = points

        if tile.animation:
            key_frame_list = []
            for frame in tile.animation:
                frame_tile = self._get_tile_by_id(tile.tileset, frame.tile_id)
                if frame_tile:
                    image_file = _get_image_source(frame_tile, map_directory)

                    if frame_tile.image and image_file:
                        texture = load_texture(image_file)
                    elif not frame_tile.image and image_file:
                        # No image for tile, pull from tilesheet
                        (
                            image_x,
                            image_y,
                            width,
                            height,
                        ) = _get_image_info_from_tileset(frame_tile)

                        texture = load_texture(
                            image_file, image_x, image_y, width, height
                        )
                    else:
                        raise RuntimeError(
                            f"Warning: failed to load image for animation frame for "
                            f"tile '{frame_tile.id}', '{image_file}'."
                        )

                    key_frame = AnimationKeyframe(  # type: ignore
                        frame.tile_id, frame.duration, texture
                    )
                    key_frame_list.append(key_frame)

                    if len(key_frame_list) == 1:
                        my_sprite.texture = key_frame.texture

            cast(AnimatedTimeBasedSprite, my_sprite).frames = key_frame_list

        return my_sprite

def load_tilemap(
    map_file: Union[str, Path],
    scaling: float = 1.0,
    layer_options: Optional[Dict[str, Dict[str, Any]]] = None,
    use_spatial_hash: Optional[bool] = None,
    hit_box_algorithm: str = "Simple",
    hit_box_detail: float = 4.5,
    offset: Vec2 = Vec2(0, 0),
    callback_progress: Callable = None
) -> IsometricTileMap:
    """
    Given a .json map file, loads in and returns a `TileMap` object.

    A TileMap can be created directly using the classes `__init__` function.
    This function exists for ease of use.

    For more clarification on the layer_options key, see the `__init__` function
    of the `TileMap` class

    :param callback_progress: function to get current loading progress
    :param Union[str, Path] map_file: The JSON map file.
    :param float scaling: The global scaling to apply to all Sprite's within the map.
    :param Optional[bool] use_spatial_hash: If set to True, this will make moving a sprite
               in the SpriteList slower, but it will speed up collision detection
               with items in the SpriteList. Great for doing collision detection
               with static walls/platforms.
    :param str hit_box_algorithm: One of 'None', 'Simple' or 'Detailed'.
    :param float hit_box_detail: Float, defaults to 4.5. Used with 'Detailed' to hit box.
    :param Dict[str, Dict[str, Any]] layer_options: Layer specific options for the map.
    :param pyglet.math.Vec2 offset: Can be used to offset the position of all sprites and objects
            within the map. This will be applied in addition to any offsets from Tiled. This value
            can be overridden with the layer_options dict.
    """
    return IsometricTileMap(
        map_file=map_file,
        scaling=scaling,
        layer_options=layer_options,
        use_spatial_hash=use_spatial_hash,
        hit_box_algorithm=hit_box_algorithm,
        hit_box_detail=hit_box_detail,
        offset=offset,
        callback_progress=callback_progress
    )
