import time

import arcade
import arcade.gui
import sys

from arcade.gui import UILabel, UIOnChangeEvent

from UI.custom_widgets import UILoadingBar, UICheckbox
from constants import TILE_SCALE, LAYER_OPTIONS, SCREEN_TITLE
from isometric.iso_tilemap import load_tilemap
from arcade.experimental.uislider import UISlider
from settings_manager import settings
import multiprocessing as mp

# from views.game import ZombieGame


def load_map_async(multi_dict):

    def loading(current_count, total):
        multi_dict["progress"] = (current_count / total) * 100

    print("Start load map...")
    start = time.time()
    tile_map = load_tilemap("tiled/map_2_s.json", TILE_SCALE, LAYER_OPTIONS, callback_progress=loading)
    end = time.time()
    print(f"Map load time: {end - start}s")
    print("Done loading map")

    multi_dict["map"] = tile_map.sprite_lists
    return tile_map


class Settings(arcade.View):

    def __init__(self):
        super().__init__()

        # UIManager to handle the UI.
        self.manager = arcade.gui.UIManager()
        self.manager.enable()

        # Used to center the UI elements
        self.v_box = arcade.gui.UIBoxLayout()

        arcade.set_background_color(arcade.color.DARK_SLATE_BLUE)

        label_box = arcade.gui.UIBoxLayout(vertical=False)
        fullscreen_button = UICheckbox(width=30, height=30, value=settings['fullscreen'])
        label = UILabel(text=f"Fullscreen: ")
        label_box.add(label.with_space_around(bottom=22))
        label_box.add(fullscreen_button.with_border(color=arcade.csscolor.WHITE).with_space_around(bottom=20))
        self.v_box.add(label_box)

        ui_slider = UISlider(value=settings['volume'] * 100, width=300, height=50)
        label = UILabel(text=f"Volume: {ui_slider.value:02.0f}%")

        @ui_slider.event()
        def on_change(event: UIOnChangeEvent):
            label.text = f"Volume: {ui_slider.value:02.0f}%"

            # Update volume in settings
            settings['volume'] = ui_slider.value / 100

            # Update ui
            label.fit_content()

        @fullscreen_button.event()
        def on_change(event: UIOnChangeEvent):
            # Update volume in settings
            settings['fullscreen'] = event.new_value
            self.window.set_fullscreen(event.new_value)

        self.v_box.add(ui_slider)
        self.v_box.add(label.with_space_around(bottom=100))

        back_button = arcade.gui.UIFlatButton(text="Back", width=200)
        self.v_box.add(back_button.with_space_around(bottom=20))

        back_button.on_click = self.on_click_back

        # Create a widget to hold the v_box widget, that will center the buttons
        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x",
                anchor_y="center_y",
                child=self.v_box)
        )

    def on_click_back(self, event):
        if self.window.current_view != self:
            return

        menu_view = MainMenu()
        self.window.show_view(menu_view)

    def on_show(self):
        """Called when switching to this view."""
        pass

    def on_draw(self):
        """Draw the menu"""
        self.clear()

        self.manager.draw()

    def on_update(self, delta_time: float):
        pass


class MainMenu(arcade.View):
    """Class that manages the 'menu' view."""

    def __init__(self):
        super().__init__()

        # UIManager to handle the UI.
        self.manager = arcade.gui.UIManager()
        self.manager.enable()

        # Used to center the UI elements
        self.v_box = arcade.gui.UIBoxLayout()

        arcade.set_background_color(arcade.color.DARK_BLUE_GRAY)

        self.background = arcade.load_texture("assets/menu-background.png")
        arcade.load_font("assets/fonts/Kenney Bold.ttf")

        self.menu_music = arcade.load_sound("assets/sounds/ZombieGameMenu.mp3")
        self.menu_music = self.menu_music.play(volume=0.15 * settings['volume'], loop=True)

        ui_text_label = arcade.gui.UILabel(text=SCREEN_TITLE, font_name="Kenney Bold", font_size=32)
        self.v_box.add(ui_text_label.with_space_around(bottom=128))

        # Create the buttons
        start_button = arcade.gui.UIFlatButton(text="Start Game", width=200)
        self.v_box.add(start_button.with_space_around(bottom=20))

        settings_button = arcade.gui.UIFlatButton(text="Settings", width=200)
        self.v_box.add(settings_button.with_space_around(bottom=20))

        quit_button = arcade.gui.UIFlatButton(text="Quit", width=200)
        self.v_box.add(quit_button.with_space_around(bottom=20))

        self.loading_bar = UILoadingBar(width=512, height=32)
        self.v_box.add(self.loading_bar.with_border(2, arcade.csscolor.WHITE).with_space_around(128))

        # Clicks events
        start_button.on_click = self.on_click_start
        settings_button.on_click = self.on_click_settings
        quit_button.on_click = self.on_click_exit

        # Create a widget to hold the v_box widget, that will center the buttons
        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="center_x",
                anchor_y="center_y",
                child=self.v_box)
        )

        self.p = None
        if sys.platform == "darwin":
            # Weird problems with multiprocessing on Mac so disable it
            return

        if not mp.get_context('spawn').active_children():
            mp.set_start_method('spawn')

        self.mp_manager = mp.Manager()

        self.m_dict = self.mp_manager.dict()
        self.m_dict["map"] = None
        self.m_dict["progress"] = 0
        self.p = mp.Process(target=load_map_async, args=(self.m_dict,))
        self.p.start()

    def on_click_settings(self, event):
        if self.window.current_view != self:
            return

        # Stop load map in settings
        self.p.kill()

        # Pause music
        self.menu_music.pause()

        settings_view = Settings()
        self.window.show_view(settings_view)

    def on_close(self):
        if self.p is not None:
            self.p.kill()

    def on_click_start(self, event):
        if self.window.current_view != self:
            return

        if sys.platform == "darwin":
            # If mac don't use multiprocessing
            tile_map_sprites = load_tilemap("tiled/map_2_s.json", TILE_SCALE, LAYER_OPTIONS, callback_progress=None).sprite_lists

        else:
            # Get tile map from multiprocess shared memory
            tile_map_sprites = self.m_dict["map"]

        if tile_map_sprites is None:
            return

        from views.game import ZombieGame
        arcade.sound.stop_sound(self.menu_music)
        game_view = ZombieGame()
        game_view.setup(tile_map_sprites)
        self.window.show_view(game_view)
        self.window.switch_to()

    def on_click_exit(self, event):
        if self.window.current_view != self:
            return

        # Kill multiprocess if quit while loading the map in the background
        if self.p is not None and self.p.is_alive():
            self.p.kill()

        # Exit arcade
        arcade.exit()

    def on_show(self):
        """Called when switching to this view."""
        pass

    def on_draw(self):
        """Draw the menu"""
        self.clear()

        arcade.draw_lrwh_rectangle_textured(0, 0, self.manager.rect.width, self.manager.rect.height, self.background)

        self.manager.draw()

    def on_update(self, delta_time: float):
        if self.p is None:
            return

        if not self.p.is_alive():
            self.p.kill()

        self.loading_bar.set_progress(self.m_dict["progress"])

    def on_resize(self, width: int, height: int):
        self.manager.on_resize(width, height)
