from random import randint
from typing import Optional

import arcade
from arcade import Texture
from arcade.gui import UIInteractiveWidget, Surface, UIBorder, UIWidget, UIWrapper, UIEvent, UIMousePressEvent, \
    UIOnChangeEvent
from pyglet.event import EVENT_HANDLED, EVENT_UNHANDLED


class UICheckbox(UIInteractiveWidget):
    """
    UI Checkbox

    :param float x: x coordinate of bottom left
    :param float y: y coordinate of bottom left
    :param color: fill color for the widget
    :param width: width of widget
    :param height: height of widget
    :param size_hint: Tuple of floats (0.0-1.0), how much space of the parent should be requested
    :param size_hint_min: min width and height in pixel
    :param size_hint_max: max width and height in pixel
    :param style: not used
    """

    def __init__(self, x=0, y=0, width=100, height=100, value=False, unchecked_color=arcade.csscolor.GRAY, checked_color=arcade.csscolor.GREEN,
                 size_hint=None,
                 size_hint_min=None,
                 size_hint_max=None,
                 style=None,
                 **kwargs):
        super().__init__(x, y, width, height,
                         size_hint=size_hint,
                         size_hint_min=size_hint_min,
                         size_hint_max=size_hint_max)
        self.unchecked_color = unchecked_color
        self.checked_color = checked_color

        self.checked = value

        self.register_event_type("on_change")

    def do_render(self, surface: Surface):
        self.prepare_render(surface)

        if self.checked:
            surface.clear((self.checked_color[0], self.checked_color[1], self.checked_color[2]))
        else:
            surface.clear((self.unchecked_color[0], self.unchecked_color[1], self.unchecked_color[2]))

    def on_change(self, event: UIOnChangeEvent):
        pass

    def on_event(self, event: UIEvent) -> Optional[bool]:
        # if not active, check to activate, return
        if isinstance(event, UIMousePressEvent):
            if self.rect.collide_with_point(event.x, event.y):
                self.checked = not self.checked
                self.dispatch_event("on_change", UIOnChangeEvent(self, not self.checked, self.checked))  # type: ignore
                self.trigger_full_render()
                return EVENT_HANDLED

        if super().on_event(event):
            return EVENT_HANDLED

        return EVENT_UNHANDLED


class UILoadingBar(UIInteractiveWidget):
    """
    Solid color widget, used for testing.

    :param float x: x coordinate of bottom left
    :param float y: y coordinate of bottom left
    :param color: fill color for the widget
    :param width: width of widget
    :param height: height of widget
    :param size_hint: Tuple of floats (0.0-1.0), how much space of the parent should be requested
    :param size_hint_min: min width and height in pixel
    :param size_hint_max: max width and height in pixel
    :param style: not used
    """

    def __init__(self, x=0, y=0, width=100, height=100, color=arcade.color.BLACK, back_color=arcade.csscolor.GRAY,
                 size_hint=None,
                 size_hint_min=None,
                 size_hint_max=None,
                 style=None,
                 **kwargs):
        super().__init__(x, y, width, height,
                         size_hint=size_hint,
                         size_hint_min=size_hint_min,
                         size_hint_max=size_hint_max)

        self.color = color
        self.progress: int = 0
        self.full: bool = False

    def do_render(self, surface: Surface):
        self.prepare_render(surface)

        surface.clear()

        # Clamps the value
        perc = max(min((self.progress / 100), 1.0), 0.0)

        surface.limit(self.rect.x, self.rect.y, width=perc * self.width, height=self.height)
        surface.clear((self.color[0], self.color[1], self.color[2], 256))


    def on_update(self, dt):
        pass

    def set_progress(self, progress: int):
        """
        Sets the current progress of the loading bar
        :param int progress: 0 - 100
        """

        if self.full and progress == 100:
            # Prevent triggering render if progress is equal to 100
            return

        self.progress = progress
        self.trigger_render()

        self.full = True if progress == 100 else False







