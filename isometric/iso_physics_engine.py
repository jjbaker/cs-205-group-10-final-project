import math
from typing import Iterable, List, Optional, Union

from arcade import (Sprite, SpriteList, check_for_collision,
                    check_for_collision_with_lists, get_distance, scene)


# Code modified from arcade.physics_engines
from arcade.physics_engines import _move_sprite


class PhysicsEngineGame:
    """
    Simplistic physics engine for use in a platformer. It is easier to get
    started with this engine than more sophisticated engines like PyMunk.

    **Note:** Sending static sprites to the ``walls`` parameter and moving sprites to the
    ``dynamics`` parameter will have very extreme benefits to performance.

    :param Sprite player_sprite: The moving sprite
    :param Optional[Union[SpriteList, Iterable[SpriteList]]] dynamic_layers: Sprites the player can't move through.
        This value should only be used for moving Sprites. Static sprites should be sent to the ``walls`` parameter.
    :param float gravity_constant: Downward acceleration per frame
    :param Optional[Union[SpriteList, Iterable[SpriteList]]] ladders: Ladders the user can climb on
    :param Optional[Union[SpriteList, Iterable[SpriteList]]] walls_layers: Sprites the player can't move through.
        This value should only be used for static Sprites. Moving sprites should be sent to the ``platforms`` parameter.
    """

    def __init__(self,
                 scene: scene,
                 player_sprite: Sprite,
                 dynamic_layers: Optional[Union[str, List[str]]] = None,
                 walls: Optional[Union[SpriteList, Iterable[SpriteList]]] = None,
                 ):
        """
        Create a physics engine for a platformer.
        """
        self.scene: scene = scene

        self.dynamic_layers: List[str]

        self.walls: List[SpriteList]

        if dynamic_layers:
            if isinstance(dynamic_layers, str):
                self.dynamic_layers = [dynamic_layers]
            else:
                self.dynamic_layers = list(dynamic_layers)
        else:
            self.dynamic_layers = []

        if walls:
            if isinstance(walls, SpriteList):
                self.walls = [walls]
            else:
                self.walls = list(walls)
        else:
            self.walls = []

        self.player_sprite: Sprite = player_sprite

        self.dynamics = []

    def update(self):
        """
        Move everything and resolve collisions.

        :Returns: SpriteList with all sprites contacted. Empty list if no sprites.
        """

        # TODO Might just want to add a method to add sprite to the physics engine instead of getting the sprites directly from the scene
        # Surprisingly efficient https://stackoverflow.com/questions/12088089/python-list-concatenation-efficiency
        self.dynamics.clear()
        for layer in self.dynamic_layers:
            self.dynamics[0:0] = self.scene.get_sprite_list(layer)

        complete_hit_list = _move_sprite(self.player_sprite, self.walls, ramp_up=False)

        # Update position of all dynamic sprites
        for dynamic_sprite in self.dynamics:
            _move_sprite(dynamic_sprite, self.walls, ramp_up=False)

        return complete_hit_list
