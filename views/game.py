import math
import time

import arcade

from constants import DEBUG, LAYER_NAME_ENEMIES, LAYER_NAME_PLAYER, LAYER_NAME_BULLETS, LAYER_NAME_GUN
from enemy_control import EnemyControl
from isometric.iso_physics_engine import PhysicsEngineGame
from player_control import PlayerControl
from UI.in_game_ui import GameWidgets
from settings_manager import settings


class ZombieGame(arcade.View):
    """
    Main application class.
    """

    def __init__(self):

        # Call the parent class and set up the window
        super().__init__()

        self.quit_time = None
        self.quit_game = None
        arcade.set_background_color(arcade.csscolor.CORNFLOWER_BLUE)

        self.tile_list = None

        self.player_control = None

        self.physics_engine = None

        # Our TileMap Object
        self.tile_map = None
        self.scene = None

        self.enemy_control = None

        self.camera_sprites = arcade.Camera(self.window.width, self.window.height)
        self.camera_gui = arcade.Camera(self.window.width, self.window.height)

        self.game_widgets = GameWidgets(self.camera_gui)

    def setup(self, tile_map):
        """Set up the game here"""
        # Get the tile map from the main menu async loading
        self.tile_map = tile_map

        if DEBUG:
            arcade.enable_timings()

        # Initialize Scene with our TileMap, this will automatically add all layers
        # from the map as SpriteLists in the scene in the proper order.
        # self.scene = arcade.Scene.from_tilemap(self.tile_map)
        self.scene = arcade.Scene()
        for name, sprite_list in tile_map.items():
            self.scene.add_sprite_list(name=name, sprite_list=sprite_list)

        self.player_control = PlayerControl(self.scene, self.camera_sprites)
        self.scene.add_sprite_list_before(LAYER_NAME_ENEMIES, LAYER_NAME_PLAYER)
        self.scene.add_sprite_list_before(LAYER_NAME_BULLETS, LAYER_NAME_ENEMIES)

        # Pass player to Game widgets
        self.game_widgets.input_player_control(self.player_control)

        self.enemy_control = EnemyControl(self.scene, self.player_control.player)

        # Create the 'physics engine'
        self.physics_engine = PhysicsEngineGame(
            self.scene,
            self.player_control.player,
            dynamic_layers=[LAYER_NAME_ENEMIES],
            walls=self.scene["Walls"]
        )

        self.ambiance = arcade.load_sound("assets/sounds/Cavernous Desert Wind.mp3")
        self.ambiance = self.ambiance.play(volume=0.25 * settings['volume'], loop=True)

    def on_draw(self):
        """Render the screen."""

        self.clear()
        # Code to draw the screen goes here

        # Select the camera we'll use to draw all our sprites
        self.camera_sprites.use()

        # Draw our Scene
        self.scene.draw(pixelated=True)

        # self.scene["Walls"].draw_hit_boxes()
        # self.scene[LAYER_NAME_PLAYER].draw_hit_boxes()

        # Select the (unscrolled) camera for our GUI
        self.camera_gui.use()

        self.game_widgets.on_draw()

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        self.player_control.on_key_press(key, modifiers)

    def on_mouse_press(self, x, y, button, modifiers):
        self.player_control.on_mouse_press(x, y, button, modifiers)

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        self.player_control.on_key_release(key, modifiers)

        if key == arcade.key.F:
            self.enemy_control.spawn_zombie( (self.player_control.player.position[0], self.player_control.player.position[1] + 32))

        if key == arcade.key.RIGHT:
            self.player_control.player.add_health()

        if key == arcade.key.LEFT:
            self.player_control.player.health -= 1

    def on_update(self, delta_time):
        """ Movement and game logic """

        self.player_control.update()

        self.game_widgets.update_kill_count(self.enemy_control.kill_count)

        self.scene.update(
            [LAYER_NAME_PLAYER, LAYER_NAME_BULLETS]
        )

        # Also updates each zombie
        self.enemy_control.update()

        # Update Animations
        self.scene.update_animation(
            delta_time, [LAYER_NAME_PLAYER, LAYER_NAME_ENEMIES, ]
        )

        # Call update on all sprites (The sprites don't do much in this
        # example though.)
        self.physics_engine.update()

        # Scroll the screen to the player
        self.player_control.scroll_to_player(self.window.width, self.window.height)

        # If player health == 0 return to main menu
        if self.player_control.player.health <= 0:
            if not self.quit_game:
                self.quit_time = time.time()
            self.quit_game = True
            self.game_widgets.game_over()

        # If player has run out of health, exit game after 3 seconds
        if self.quit_time:
            if time.time() - self.quit_time > 5:
                exit()

    def on_resize(self, width, height):
        """
        Resize window
        Handle the user grabbing the edge and resizing the window.
        """
        self.camera_sprites.resize(width, height)
        self.camera_gui.resize(width, height)