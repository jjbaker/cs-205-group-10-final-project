<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.8" tiledversion="1.8.2" name="marker_s_tileset" tilewidth="128" tileheight="256" tilecount="4" columns="4">
 <image source="marker_tiles_s.png" width="512" height="256"/>
 <tile id="0">
  <properties>
   <property name="type_marker" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="type_marker" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="tye_marker" type="int" value="2"/>
  </properties>
 </tile>
</tileset>
