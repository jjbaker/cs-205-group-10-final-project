import time
from isometric.iso_sprite import Direction, FrameSequence, State, SpriteSheetData, IsoSprite


"""
    Class generates the gun for player used in player_control.py
    Gun utilizes features of parent class IsoSprite to set direction
    and correct frame from sprite sheet. Additional movement is added
    to gun to simulate walking effect of player as well as angles
    corresponding to the latest bullet fired.
    
    Child of IsoSprite
"""

# Frames state order
PLAYER_ANIM_FRAME_ORDER = [
    FrameSequence(1, State.Idle)

]
# Gun animation order,
PLAYER_ANIM_DIRECTIONS_ORDER = [
    Direction.W, Direction.E, Direction.SE, Direction.SW, Direction.NW, Direction.NE, Direction.S, Direction.N
]


class Gun(IsoSprite):

    def __init__(self, direction: Direction):

        super().__init__((250, 250), SpriteSheetData("assets/gun_sprites_lefty.png", PLAYER_ANIM_FRAME_ORDER,
                                                     PLAYER_ANIM_DIRECTIONS_ORDER, col_count=2, frame_duration=100), (256, 128), scale=.2)

        # set gun starting direction
        # And other fields
        self.previous_direction = None
        self.current_direction = direction
        self.animation_cnt = 0
        self.arm_forward = True
        self.moving = False
        self.angle_timer = time.time() - 5
        self.current_direction = None
        self.movement_change = False

    def set(self, center_x, center_y, direction: Direction, player_state: State):
        """Set gun direction based on player direction"""

        self.center_x = center_x
        self.center_y = center_y
        if direction != self.current_direction:
            self.movement_change = True
        self.current_direction = direction

        # Logic to move gun with player movement
        if not player_state == State.Idle:
            self.moving = True
            stride_length = 20
            if self.arm_forward:
                self.animation_cnt += 1
                if self.animation_cnt > stride_length:
                    self.arm_forward = False

            elif not self.arm_forward:
                self.animation_cnt -= 1
                if self.animation_cnt == 0:
                    self.arm_forward = True
        else:
            self.moving = False

        if time.time() - self.angle_timer > 1:
            reset_angle = True
        elif self.previous_direction != self.current_direction:
            reset_angle = True
        else:
            reset_angle = False

        # Remove gun from player if player is in running state
        if player_state == State.Run:
            self.current_direction = Direction.N

        # Reset angle
        if reset_angle:
            self.angle = 0
        # Adjust on position
        if self.current_direction == Direction.W:
            self.center_y -= 12
            self.center_x -= 6
            if self.moving:
                if self.arm_forward:
                    self.center_x -= 3
                else:
                    self.center_x += 1
        elif self.current_direction == Direction.E:
            self.center_y -= 12
            self.center_x += 6
            if self.moving:
                if self.arm_forward:
                    self.center_x += 3
                else:
                    self.center_x -= 1
        elif self.current_direction == Direction.NW:
            self.center_y -= 5
            self.center_x -= 5
            self.angle = -35
        elif self.current_direction == Direction.NE:
            self.center_y += 5
            self.center_x += 20
            self.angle = -145
        elif self.current_direction == Direction.SW:
            self.center_y -= 15
            self.center_x -= 12
            self.angle = 35
        elif self.current_direction == Direction.SE:
            self.center_y -= 17
            self.center_x += 23
            self.angle = 145
        elif self.current_direction == Direction.S:
            self.center_y -= 22
            self.center_x += 18
            if self.moving:
                if self.arm_forward:
                    self.center_x -= 3
                else:
                    self.center_x += 3

        # Record players previous direction
        self.previous_direction = self.current_direction

    def set_angle(self, angle):
        """Set gun angle accordingly"""

        if self.current_direction == Direction.W:
            self.angle = angle
            self.angle += 180
        elif self.current_direction != Direction.S:
            self.angle = angle
        self.angle_timer = time.time()


