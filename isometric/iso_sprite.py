import math
from dataclasses import dataclass
from typing import Dict, List, Tuple

import arcade
from arcade import Point

from isometric.utils import cast_to_iso

from enum import Enum


# Might be useful
class Direction(Enum):
    N = 0
    S = 1
    W = 2
    E = 3
    NE = 4
    NW = 5
    SE = 6
    SW = 7


class State(Enum):
    Idle = 0
    Walking = 1
    Death = 2
    Attack = 3
    Run = 4
    Spawn = 5


@dataclass()
class AnimationFrame:
    duration: int
    texture: arcade.Texture


@dataclass()
class FrameSequence:
    count: int
    state: State


@dataclass()
class SpriteSheetData:
    path: str
    frames_order: List[FrameSequence]
    directions_order: List[Direction]
    col_count: int
    frame_duration: int


class IsoSprite(arcade.Sprite):

    def __init__(self, position: Tuple[int, int], spritesheet_data: SpriteSheetData, sprite_size: Tuple[int, int], scale=1):
        # Set up sprite
        super().__init__()

        self.scale = scale

        # Dictionary to store the different animation linked to state
        # key: Tuple[Direction, State], value: List[AnimationFrame]
        self.animations: Dict[Tuple[Direction, State], List[AnimationFrame]] = {}
        self.current_frame = 0
        self.time_counter = 0.0

        # Used to lock state until animation is done
        self.state_locked = False

        # --- Load Animations ---
        frames_order = spritesheet_data.frames_order
        directions_order = spritesheet_data.directions_order

        # Loop through all animation states and each sprite sheet
        current_col = 0
        current_row = 0
        for direction in directions_order:

            for frame_sequence in frames_order:
                key = (direction, frame_sequence.state)

                self.animations[key] = []

                for i in range(frame_sequence.count):
                    # print("Col: ", current_col)
                    # print("Row: ", current_row)
                    # print(f"x: {current_col * sprite_size[0]}")
                    # print(f"y: {current_row * sprite_size[1]}")

                    self.animations[key].append(
                        AnimationFrame(duration=spritesheet_data.frame_duration, texture=arcade.load_texture(spritesheet_data.path,
                                                                                 x=current_col * sprite_size[0],
                                                                                 y=current_row * sprite_size[1],
                                                                                 width=sprite_size[0],
                                                                                 height=sprite_size[1]
                                                                                 ))
                    )

                    current_col += 1

                    if current_col % spritesheet_data.col_count == 0:
                        current_row += 1  # increment row
                        current_col = 0  # reset col

        # --- Set Position ---
        # Implies all frames all the same size
        self.sprite_width = sprite_size[0]
        self.sprite_height = sprite_size[1]

        # x, y, w = cast_to_iso(position[0], position[1], self.sprite_width, self.sprite_height, scale)
        self.center_x = position[0]
        self.center_y = position[1]

        # Set the initial state
        self.current_state = State.Idle

        # Set the current direction
        self.current_direction = Direction.N

        # Set hit box, default hit box to the size of the sprite
        p1 = (-self.sprite_width / 2, -self.sprite_height / 2)
        p2 = (self.sprite_width / 2, -self.sprite_height / 2)
        p3 = (self.sprite_width / 2, self.sprite_height / 2)
        p4 = (-self.sprite_width / 2, self.sprite_height / 2)

        self.set_hit_box([p1, p2, p3, p4])

    def update_animation(self, delta_time: float = 1 / 60):
        """
        Logic for selecting the proper texture to use.
        Modified code from: AnimatedTimeBasedSprite in arcade sprite.py
        """

        key = (self.current_direction, self.current_state)
        if len(self.animations[key]) == 0:
            return

        self.time_counter += delta_time
        while self.time_counter > self.animations[key][self.current_frame].duration / 1000.0:
            self.time_counter -= self.animations[key][self.current_frame].duration / 1000.0
            self.current_frame += 1

            if self.current_frame >= len(self.animations[key]):
                # Set key to return value to update animation instantly
                key = self.on_animation_done(key)

                # Reset frame
                self.current_frame = 0

            cur_frame = self.animations[key][self.current_frame]

            self.texture = cur_frame.texture

    def on_animation_done(self, anim: Tuple[Direction, State]) -> Tuple[Direction, State]:
        """
        Called when current animation is done playing before looping to next one

        :param anim: Tuple[Direction, State]
        """
        return anim

    def set_state(self, new_state: State, lock: bool = False):

        # Sets current state if different from current
        if self.current_state != new_state:
            self.current_state = new_state

            # Set if state is locked
            self.state_locked = lock

            # Reset frame to reset animation to starting frame for new animation
            self.current_frame = 0

    # Use math.atan2(y2−y1,x2−x1) to determine direction vector
    def set_direction(self, direction_vector: Tuple[int, int]):
        # x = math.copysign(1, direction_vector[0])
        # y = math.copysign(1, direction_vector[1])

        x = direction_vector[0]
        y = direction_vector[1]

        if y > 0 and x == 0:
            self.current_direction = Direction.N
        elif y > 0 and x > 0:
            self.current_direction = Direction.NE
        elif y > 0 and x < 0:
            self.current_direction = Direction.NW

        if y < 0 and x == 0:
            self.current_direction = Direction.S
        elif y < 0 and x > 0:
            self.current_direction = Direction.SE
        elif y < 0 and x < 0:
            self.current_direction = Direction.SW

        if x > 0 and y == 0:
            self.current_direction = Direction.E
        elif x < 0 and y == 0:
            self.current_direction = Direction.W

    # Custom update method that passes in player for sprites that need player position
    def update_with_player(self, player):
        pass

    def look_at_vector(self, target: Tuple[float, float]) -> Tuple[int, int]:
        dir_x = 1 if target[0] > self.center_x else -1
        dir_y = 1 if target[1] > self.center_y else -1

        inside_offset = 20
        if target[0] + inside_offset > self.center_x > target[0] - inside_offset:
            dir_x = 0

        if target[1] + inside_offset > self.center_y > target[1] - inside_offset:
            dir_y = 0

        return (dir_x, dir_y)

    def unlock_state(self):
        self.state_locked = False

    def is_state_locked(self) -> bool:
        return self.state_locked

    @property
    def position(self) -> Point:
        """
        Get the center x and y coordinates of the sprite.

        Returns:
            (center_x, center_y)
        """
        return self._position

    @position.setter
    def position(self, new_value: Point):
        """
        Set the center x and y coordinates of the sprite.

        :param Point new_value: New position.
        """

        if new_value[0] != self._position[0] or new_value[1] != self._position[1]:
            self.clear_spatial_hashes()
            self._point_list_cache = None
            self._position = new_value
            self.add_spatial_hashes()

            for sprite_list in self.sprite_lists:
                sprite_list.update_location(self)
