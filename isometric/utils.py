from math import floor

import arcade
from arcade import Color, SpriteList

from constants import *


def cast_to_iso(screen_x: float, screen_y: float, width: int, height: int, scale: float, mods: tuple = (0, 0, 0)):
    """
    Pass in mouse x and y
    screen width/height? pass in -> (50x50)
    scale = map scaling in constants
    mods pass in None


    Casts the inputted Euclidean x and y co-ordinates to the equivalent isometric x, y, w co-ordinates
    :param screen_x: The Euclidean X that is to be cast to Isometric.
    :param screen_y: The Euclidean Y that is to be cast to Isometric.
    :param mods: A tuple of 3 floats that are the x, y, and w mods.
    :return: the isometric x, y, w found.
    """

    screen_x -= width / 2
    screen_y -= height / 2

    # because the sprites are already cast to the ~30 degrees for the isometric the only needed rotations is the
    # 45 degrees. However since cos and sin 45 are both 0.707 they are removed from the system as it simply makes
    # the cast smaller
    iso_x = (screen_x - screen_y) * ((TILE_WIDTH * scale) / 2) + mods[0] * scale
    iso_y = -(screen_x + screen_y) * ((TILE_HEIGHT * scale) / 2) + mods[1] * scale
    iso_w = screen_x + screen_y + mods[2]

    # return the calculated values.
    return iso_x, iso_y, iso_w


def cast_from_iso(tile_x: int, tile_y: int, width: int, height: int, tile_width: float, tile_height: float,
                  scale: float):
    relative_x = tile_x / (tile_width * scale) - tile_y / (tile_height * scale) + 1
    relative_y = -tile_x / (tile_width * scale) - tile_y / (tile_height * scale) + 1

    map_width, map_height = width, height

    relative_x += map_width / 2
    relative_y += map_height / 2

    return floor(relative_x), floor(relative_y)