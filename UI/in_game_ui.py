import math
import time
import arcade
import arcade.gui
import datetime
import constants
from UI.custom_widgets import UILoadingBar
from isometric.bullet import Bullet

"""
    Class that manages the the UI elements of 
    the game including:
    -> Player health bar
    -> Remaining ammo / or reloading text
    -> Player survival timer
    
    Child of arcade.View
"""


class GameWidgets(arcade.View):
    def __init__(self, camera_gui):
        super().__init__()

        self.end = None
        self.camera_gui = camera_gui

        # Set manager for health bar
        self.manager = arcade.gui.UIManager()
        self.manager.enable()
        self.player_control = None
        self.player = None
        self.kill_count = 0

        self.v_box = arcade.gui.UIBoxLayout()
        self.health_bar = UILoadingBar(width=300, height=22, color=arcade.csscolor.RED)
        self.v_box.add(self.health_bar.with_border(5, arcade.csscolor.BLACK).with_space_around(10, 10, 10, 10))

        # Add timer showing how long player has survived
        self.init_time = time.time()
        self.time_survived = time.time() - self.init_time

        # Record screen size in case of window change
        self.screen_width = self.camera_gui.viewport_width
        self.screen_height = self.camera_gui.viewport_height

        self.manager.add(
            arcade.gui.UIAnchorWidget(
                anchor_x="left",
                anchor_y="top",
                child=self.v_box)
        )

        # Create vector of bullets to show remaining ammo
        self.generate_bullet_list()

    def input_player_control(self, player_control):
        self.player_control = player_control
        self.player = player_control.player

    def update_kill_count(self, kill_count):
        self.kill_count = kill_count

    def generate_bullet_list(self):
        self.bullet_list = []
        x_element = 0
        for i in range(constants.CLIP_SIZE):
            bullet = Bullet(self.camera_gui.viewport_width - 390 + x_element,
                            self.camera_gui.viewport_height - self.camera_gui.viewport_height * .93,
                            None, None, None, None, True)
            self.bullet_list.append(bullet)
            x_element += 15

    def on_draw(self):
        # Only do it when health has changed since triggers render of health bar
        new_health_perc = (self.player.health / constants.PLAYER_MAX_HEALTH) * 100
        if self.health_bar.progress != new_health_perc:
            self.health_bar.set_progress(new_health_perc)

        self.time_survived = int(time.time() - self.init_time)
        survival_text = f"Survived: {datetime.timedelta(seconds=self.time_survived)}"
        arcade.draw_text(
            survival_text,
            self.camera_gui.viewport_width - 300,
            self.camera_gui.viewport_height - 45,
            arcade.csscolor.BLACK,
            22,
        )

        # Reconfigure bullet list on window change
        if self.screen_width != self.camera_gui.viewport_width or \
                self.screen_height != self.camera_gui.viewport_height:
            self.generate_bullet_list()
            self.screen_width = self.camera_gui.viewport_width
            self.screen_height = self.camera_gui.viewport_height

        # Draw ammo
        if not self.player_control.reloading:
            for i in range(constants.CLIP_SIZE - self.player_control.bullets_shot):
                self.bullet_list[i].draw()
        else:
            survival_text = f"RELOADING"
            arcade.draw_text(
                survival_text,
                self.camera_gui.viewport_width - 300,
                self.camera_gui.viewport_height - self.camera_gui.viewport_height * .96,
                arcade.csscolor.BLACK,
                28,
                font_name="Kenny Blocks"
            )

        if self.end:
            end_text = f"GAME OVER"
            arcade.draw_text(
                end_text,
                self.camera_gui.viewport_width / 2 - 140,
                self.camera_gui.viewport_height - self.camera_gui.viewport_height * .7,
                arcade.csscolor.DARK_RED,
                38,
                font_name="Kenny Blocks"
            )

        # Draw FPS
        fps_text = f"FPS: {math.floor(arcade.get_fps())}"
        arcade.draw_text(
            fps_text,
            10,
            self.camera_gui.viewport_height - 80,
            arcade.csscolor.WHITE,
            18,
        )

        # Draw Kill Count
        kill_text = f"KILLS: {self.kill_count}"
        arcade.draw_text(
            kill_text,
            self.camera_gui.viewport_width - self.camera_gui.viewport_width * .9,
            self.camera_gui.viewport_height - self.camera_gui.viewport_height * .9,
            arcade.csscolor.DARK_RED,
            25,
        )

        # Draw Health Bar
        self.manager.draw()

    def game_over(self):
        self.end = True
