import random
from constants import*
from enum import Enum
from isometric.iso_sprite import Direction, FrameSequence, State, SpriteSheetData, IsoSprite

"""
    Class generates player utilizing features of parent class IsoSprite
    Player health set and recorded 
    
    Child of IsoSprite
"""


# Frames state order
PLAYER_ANIM_FRAME_ORDER = [
    FrameSequence(1, State.Idle),
    FrameSequence(8, State.Walking),
    FrameSequence(8, State.Run)

]
# Player Direction order: NW, N, NE, W, E, SW, SE, S
PLAYER_ANIM_DIRECTIONS_ORDER = [
    Direction.NW, Direction.N, Direction.NE, Direction.W, Direction.E, Direction.SW, Direction.SE, Direction.S
]


class Action(Enum):
    IDLE = 0
    WALKING = 1
    RUN = 2
    ATTACK = 3


class Player(IsoSprite):

    def __init__(self):

        super().__init__((250, 250), SpriteSheetData("assets/animations/player-character-all.png", PLAYER_ANIM_FRAME_ORDER,
                                                     PLAYER_ANIM_DIRECTIONS_ORDER, col_count=12, frame_duration=100), (128, 208), scale=.45)

        # Initialize
        self.current_action = Action.IDLE
        self.current_direction = Direction.W

        # Player hitbox
        # Create player
        p1 = (-12, -25)
        p2 = (8, -25)
        p3 = (8, 20)
        p4 = (-12, 20)

        self.set_hit_box([p1, p2, p3, p4])
        self.health = PLAYER_MAX_HEALTH

    def player_hit(self):
        """Remove health from player on enemy hit"""

        self.health -= random.randint(2, 6)

    def add_health(self):
        """Add health to player"""

        if self.health != PLAYER_MAX_HEALTH:
            self.health += 1

