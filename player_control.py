import math
import random
import arcade
import constants
from isometric.iso_sprite import IsoSprite, Direction, State
from player import Player
from pyglet.math import Vec2
from constants import *
from gun import Gun
from isometric.bullet import Bullet
import time

from settings_manager import settings

"""
    Class handles player events including:
    -> Shooting bullets
    -> Creating and aiming gun
    -> Associated sounds
    -> Scrolling screen to player
    -> Player movements in response to mouse/key input
"""


class PlayerControl:

    def __init__(self, scene: arcade.scene, camera):

        # Set fields
        self.scene = scene
        self.camera = camera
        self.width = None
        self.height = None
        self.position = None
        self.health_interval = time.time()
        self.spawn_timer = 0

        # Create player
        self.player = Player()
        self.player.center_x = 120
        self.player.center_y = -1350
        self.PLAYER_SPEED = 3

        self.base_color = self.player.color

        # Add player to scene
        self.scene.add_sprite(LAYER_NAME_PLAYER, self.player)

        # Create gun
        self.gun = Gun(self.player.current_direction)
        self.gun.center_x = self.player.center_x
        self.gun.center_y = self.player.center_y

        # Add gun to scene
        self.scene.add_sprite(LAYER_NAME_PLAYER, self.gun)

        # Set key inputs
        self.left_pressed = False
        self.right_pressed = False
        self.up_pressed = False
        self.down_pressed = False
        self.player_direction = None

        # Create bullet list
        self.bullet_list = []
        self.score = 0
        self.bullets_shot = 0
        self.reloading = False
        self.reload_start_time = None
        self.PLAYER_SPEED = constants.PLAYER_REG_SPEED
        self.sprinting = False

        # Load sounds
        self.walk_sound = arcade.load_sound("assets/sounds/footstep.ogg")
        self.walk_sound_player = None

        self.shoot_sound = arcade.load_sound("assets/sounds/shoot.ogg")

        self.reload_sound = arcade.load_sound("assets/sounds/reload.ogg")

    def shoot(self):
        """Draw bullets when player is not reloading"""

        if not self.reloading and not self.sprinting:
            for bullet in self.bullet_list:
                if bullet.active:
                    bullet.draw()

    def on_key_press(self, key, modifiers):
        """Whenever key is pressed, set fields accordingly"""

        if key == arcade.key.W:
            self.up_pressed = True
            if not self.sprinting:
                self.player.set_state(State.Walking)
        elif key == arcade.key.S:
            self.down_pressed = True
            if not self.sprinting:
                self.player.set_state(State.Walking)
        elif key == arcade.key.A:
            self.left_pressed = True
            if not self.sprinting:
                self.player.set_state(State.Walking)
        elif key == arcade.key.D:
            self.right_pressed = True
            if not self.sprinting:
                self.player.set_state(State.Walking)
        elif key == arcade.key.LSHIFT:
            self.sprinting = True
            self.PLAYER_SPEED = constants.PLAYER_SPRINT_SPEED
            self.player.set_state(State.Run)
        elif key == arcade.key.SPACE:
            self.reloading = True
            self.reload_sound.play(0.4 * settings['volume'])
            self.reload_start_time = time.time()

    def on_key_release(self, key, modifiers):
        """When keys are released, set fields accordingly"""

        if key == arcade.key.W:
            self.up_pressed = False
        elif key == arcade.key.S:
            self.down_pressed = False
        elif key == arcade.key.A:
            self.left_pressed = False
        elif key == arcade.key.D:
            self.right_pressed = False
        elif key == arcade.key.LSHIFT:
            if not self.right_pressed and not self.left_pressed and not self.up_pressed and not self.down_pressed:
                self.player.set_state(State.Idle)
            else:
                self.player.set_state(State.Walking)

            self.sprinting = False
            self.PLAYER_SPEED = constants.PLAYER_REG_SPEED

    def on_mouse_press(self, x, y, button, modifiers):
        """Whenever mouse is pressed, shoot bullet, setting
        fields accordingly"""

        # Position the bullet at the player's current location
        start_x = self.player.center_x
        start_y = self.player.center_y

        dest_x = x
        dest_y = y

        camera_x = self.camera.viewport_width / 2
        camera_y = self.camera.viewport_height / 2

        x_diff = x - camera_x
        y_diff = y - camera_y

        # Change player direction towards shooting direction
        angle = math.degrees(math.atan2(x_diff, y_diff))
        # print(angle)
        if -18 < angle < 18:
            self.player.current_direction = Direction.N
        elif -18 > angle > -30:
            self.player.current_direction = Direction.NW
        elif -30 > angle > -146:
            self.player.current_direction = Direction.W
        elif -146 > angle > -172:
            self.player.current_direction = Direction.SW
        elif -172 > angle > -179.9 or 179.9 > angle > 172:
            self.player.current_direction = Direction.S
        elif 172 > angle > 146:
            self.player.current_direction = Direction.SE
        elif 146 > angle > 30:
            self.player.current_direction = Direction.E
        elif 30 > angle > 18:
            self.player.current_direction = Direction.NE

        if self.bullets_shot < CLIP_SIZE and not self.reloading and not self.sprinting:
            bullet = Bullet(start_x, start_y, self.camera.viewport_width,
                            self.camera.viewport_height, dest_x, dest_y, ui_element=False)
            self.bullets_shot += 1

            # Set gun angle
            self.gun.set_angle(bullet.angle)

            # Add the bullet to the appropriate lists
            self.bullet_list.append(bullet)
            self.shoot_sound.play(volume=0.1 * settings['volume'])
            self.scene.add_sprite(LAYER_NAME_BULLETS, bullet)

            # Set reloading when player uses all bullets
            if self.bullets_shot >= CLIP_SIZE:
                self.reloading = True
                self.reload_sound.play(0.4 * settings['volume'])
                self.reload_start_time = time.time()

    def update(self):
        """Update player movement, gun angles/position, bullets
        Reloading, and player health"""

        # Update gun
        self.gun.set(self.player.center_x, self.player.center_y, self.player.current_direction, self.player.current_state)

        # Calculate speed based on the keys pressed
        self.player.change_x = 0
        self.player.change_y = 0

        if self.up_pressed and not self.down_pressed:
            self.player.change_y = self.PLAYER_SPEED
        elif self.down_pressed and not self.up_pressed:
            self.player.change_y = -self.PLAYER_SPEED
        if self.left_pressed and not self.right_pressed:
            self.player.change_x = -self.PLAYER_SPEED
        elif self.right_pressed and not self.left_pressed:
            self.player.change_x = self.PLAYER_SPEED

        # if the player moves play step sound
        if self.player.change_x != 0 or self.player.change_y != 0:
            if self.walk_sound_player is None or not self.walk_sound.is_playing(self.walk_sound_player):

                # Don't play the sound at every step
                if random.randint(0, 5) > 3:
                    self.walk_sound_player = self.walk_sound.play(0.3 * settings['volume'])

        # Set player directions
        if self.right_pressed:
            if self.up_pressed:
                self.player.current_direction = Direction.NE
            elif self.down_pressed:
                self.player.current_direction = Direction.SE
            else:
                self.player.current_direction = Direction.E

        elif self.left_pressed:
            if self.up_pressed:
                self.player.current_direction = Direction.NW
            elif self.down_pressed:
                self.player.current_direction = Direction.SW
            else:
                self.player.current_direction = Direction.W

        elif self.up_pressed and not self.left_pressed or self.right_pressed:
            self.player.current_direction = Direction.N

        elif self.down_pressed and not self.left_pressed or self.right_pressed:
            self.player.current_direction = Direction.S

        if not self.right_pressed and not self.left_pressed and not self.up_pressed and not self.down_pressed \
                and not self.sprinting:
            self.player.set_state(State.Idle)

        # Check if reload time has elapsed
        if self.reloading:
            if time.time() - self.reload_start_time > constants.RELOAD_INTERVAL:
                self.reloading = False
                self.bullets_shot = 0

        # Allow bullets to fly for 1 second and then remove from list
        for bullet in self.bullet_list:
            if time.time() - bullet.shoot_time > 1:
                bullet.kill()

        # Regenerate player health by 1 every 2.5 seconds
        if time.time() - self.health_interval > constants.HEALTH_REGEN_TIME:
            self.player.add_health()
            self.health_interval = time.time()

        # Shade player red on low health
        if self.player.health < constants.LOW_HEALTH:
            self.player.color = arcade.csscolor.RED
        else:
            self.player.color = self.base_color

    def scroll_to_player(self, width, height):
        """Scroll the window to the player."""

        self.width = width
        self.height = height

        self.position = Vec2(self.player.center_x - self.width / 2,
                             self.player.center_y - self.height / 2)
        self.camera.move_to(self.position, CAMERA_SPEED)
