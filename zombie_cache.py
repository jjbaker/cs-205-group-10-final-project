from collections import OrderedDict


class ZombieCache:

    # Cache is an OrderedDict object
    def __init__(self):
        self.cache = OrderedDict()

    # Retrieve key value queried from cache
    # Return -1 if not found, otherwise move key to the end to show it was used
    def get_from_cache(self, key: int) -> int:
        if key not in self.cache:
            return -1
        else:
            self.cache.move_to_end(key)
            return self.cache[key]

    # add the key
    # and also move key to end to show it was used
    def put_in_cache(self, key: int, value: int) -> None:
        self.cache[key] = value
        self.cache.move_to_end(key)
        if len(self.cache) > self.cap:
            self.cache.popitem(last=False)

    # Cache cap
    def set_cache_cap(self, cap):
        self.cap = cap
