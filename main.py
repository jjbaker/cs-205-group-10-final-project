import os
import sys
from multiprocessing import freeze_support

import arcade
import arcade.gui

from constants import *

from views.main_menu import MainMenu


def main():
    """Main function"""
    # Required for resources to be found when game is bundled with pyinstaller
    # Code from https://api.arcade.academy/en/latest/tutorials/bundling_with_pyinstaller/index.html
    if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
        os.chdir(sys._MEIPASS)

    # Required for the multiprocessing to work when we bundle the game with pyinstaller
    freeze_support()

    # Create base window
    # https://github.com/pyglet/pyglet/issues/225  # MacOS bug where sometimes window is not focused, pyglet issue can't fix it here
    window = arcade.Window(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE, resizable=True, center_window=True)

    print(f'Arcade Version: {arcade.version.VERSION}')

    # Switches to main menu
    menu_view = MainMenu()
    window.show_view(menu_view)
    arcade.run()


if __name__ == "__main__":
    main()
